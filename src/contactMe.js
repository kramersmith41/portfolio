import emailjs from '@emailjs/browser';

const form = document.getElementById('contact-form');
const button = document.getElementById('button')

function showToast(duration = 3000) {
  const toast = document.getElementById('toast');
  toast.classList.add('show');
  setTimeout(() => {
    toast.classList.remove('show');
  }, duration);
}


const sendEmail = (event) => {
  event.preventDefault();

  const formData = new FormData(form);
  const data = {
    user_name: formData.get('user_name'),
    user_email: formData.get('user_email'),
    message: formData.get('message')
  };

  emailjs.send('3DPortfolio', 'template_r52garb', data, '-6mDFzP5blzaZVGv3')
    .then((result) => {
        if (result.status === 200) {
          form.reset();
          showToast();
        }
    }, (error) => {
        console.log(error.text);
    });
};

form.addEventListener('submit', (event) => sendEmail(event));
