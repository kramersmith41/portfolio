import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import Stats from 'three/examples/jsm/libs/stats.module'
import * as THREE from 'three'
import gsap from 'gsap'

const introElement = document.getElementById('intro')
const thawdElement = document.getElementById('thawd')
const hiCrush2017Element = document.getElementById('hi-crush-2017')
const preferredSandsElement = document.getElementById('preferred-sands')
const hiCrush2020Element = document.getElementById('hi-crush-2020')
const homeElement = document.getElementById('home')
let loaded = false

/**
 * Mobile
 */
let isMobile = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}


window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Update all materials
 */
const updateAllMaterials = () => {
    scene.traverse((child) => {
        if (child instanceof THREE.Mesh && child.material instanceof THREE.MeshStandardMaterial){
            child.material.envMap = environmentMap
            child.material.envMapIntensity = 0.75
            child.castShadow = true
            child.receiveShadow = true
            child.material.needsUpdate = true
        }
    })
}

/**
 * Loaders
 */
const loadingBarElement = document.querySelector('.loading-bar')

const loadingManager = new THREE.LoadingManager(
    // Loaded
    () => {
        gsap.delayedCall(0.5, () => {
            gsap.to(overlayMaterial.uniforms.uAlpha, { duration: 3, value: 0 })
            loadingBarElement.classList.add('ended')
            loadingBarElement.style.transform = ''
            document.body.style.overflow = 'scroll'
            window.scrollTo(0,0)
            loaded = true
            updateAllMaterials()
        })
    },
    // Progress
    (itemUrl, itemsLoaded, itemsTotal) => {
        const progressRatio = itemsLoaded / itemsTotal
        loadingBarElement.style.transform = `scaleX(${progressRatio})`
    }
)

const cubeTextureLoader = new THREE.CubeTextureLoader()

const textureLoader = new THREE.TextureLoader()

const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('/draco/')

const gltfLoader = new GLTFLoader(loadingManager)
gltfLoader.setDRACOLoader(dracoLoader)

/**
 * Base
 */
// Debug

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Overlay
 */
const overlayGeometry = new THREE.PlaneGeometry(2, 2, 1, 1)
const overlayMaterial = new THREE.ShaderMaterial({
    transparent: true,
    uniforms: {
        uAlpha: { value: 1 }
    },
    vertexShader: `
        void main() {
            gl_Position = vec4(position, 1.0);
        }
    `,
    fragmentShader: `
    uniform float uAlpha;

    void main(){
        gl_FragColor = vec4(0.0, 0.0, 0.0,  uAlpha);
    }
    `
 })
const overlay = new THREE.Mesh(overlayGeometry, overlayMaterial)
scene.add(overlay)

/**
 * Environment Map
 */
let environmentMap
if (isMobile()) {
    environmentMap = cubeTextureLoader.load([
        '/textures/environmentMaps/mobile/px.png',
        '/textures/environmentMaps/mobile/nx.png',
        '/textures/environmentMaps/mobile/py.png',
        '/textures/environmentMaps/mobile/ny.png',
        '/textures/environmentMaps/mobile/pz.png',
        '/textures/environmentMaps/mobile/nz.png',
    ])
} else {
    environmentMap = cubeTextureLoader.load([
        '/textures/environmentMaps/desktop/px.png',
        '/textures/environmentMaps/desktop/nx.png',
        '/textures/environmentMaps/desktop/py.png',
        '/textures/environmentMaps/desktop/ny.png',
        '/textures/environmentMaps/desktop/pz.png',
        '/textures/environmentMaps/desktop/nz.png',
    ])
}
scene.background = environmentMap
environmentMap.encoding = THREE.sRGBEncoding

/**
 * Textures
 */
const hackReactorImage = textureLoader.load('/textures/hackReactor.jpg')
const emailMeImage = textureLoader.load('/textures/emailMe.png')
emailMeImage.flipY = false
const verticleMonitorImage = textureLoader.load('/textures/verticleScreen.jpg')


/**
 * Models
 */
let truck = new THREE.Mesh(
    new THREE.BoxGeometry(1,1,1),
    new THREE.MeshBasicMaterial()
)

let frontWheels = new THREE.Mesh(
    new THREE.BoxGeometry(1,1,1),
    new THREE.MeshBasicMaterial()
)

let backWheels = new THREE.Mesh(
    new THREE.BoxGeometry(1,1,1),
    new THREE.MeshBasicMaterial()
)

let billboard = new THREE.Mesh(
    new THREE.PlaneGeometry(1,1),
    new THREE.MeshBasicMaterial()
)

gltfLoader.load(
    '/models/scene.glb',
    (gltf) =>
    {
        truck = gltf.scene.children[1908]

        frontWheels = gltf.scene.children[1909]

        frontWheels.position.z = 11.93

        backWheels = gltf.scene.children[1910]

        billboard = gltf.scene.children[1410]
        billboard.material = new THREE.MeshBasicMaterial({ map: emailMeImage })

        // console.log("This is gltf: ", gltf);
        // console.log(gltf.scene.children.filter((child) => {
        //     if (child.name == "truck") {
        //         return gltf.scene.children.indexOf(child)
        //     }
        // }))


        scene.add(gltf.scene)

        updateAllMaterials()
    }
)

/**
 * Objects
 */
const computerScreen = new THREE.Mesh(
    new THREE.PlaneGeometry(0.48, 0.22),
    new THREE.MeshBasicMaterial({ map: hackReactorImage})
)
computerScreen.position.set(205.607, 0.775, -0.6854)
scene.add(computerScreen)

let verticleMonitorScreen = new THREE.Mesh(
    new THREE.PlaneGeometry(0.20117540475749997,0.4691392074824873),
    new THREE.MeshBasicMaterial({ map: verticleMonitorImage})
)
verticleMonitorScreen.position.set(
    205.9749,
    0.85,
    -0.6548
    )
verticleMonitorScreen.rotation.y = -0.2576
scene.add(verticleMonitorScreen)

/**
 * Lights
 */
// Directional Light
const directionalLight = new THREE.DirectionalLight(0x00fffc, 1.5)
directionalLight.position.set(26.76, 60, 60)
directionalLight.castShadow = true
directionalLight.shadow.mapSize.width = 2048
directionalLight.shadow.mapSize.height = 2048
directionalLight.shadow.mapSize.set(1024, 1024)
directionalLight.shadow.bias = -0.007
directionalLight.shadow.normalBias = 0.3
directionalLight.shadow.camera.top = 50
directionalLight.shadow.camera.bottom = - 70
directionalLight.shadow.camera.right = 220
directionalLight.shadow.camera.left = -15
directionalLight.shadow.camera.far = 100
scene.add(directionalLight)

//Garage Light
const garageLight = new THREE.PointLight(0xffffff, 0.3)
garageLight.position.set(205, 0.64, 0.33)
garageLight.shadow.camera.far = 5
scene.add(garageLight)

/**
 * Camera
 */
// Base camera
export const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.set(3, 2.5, 16)
scene.add(camera)

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.physicallyCorrectLights = true
renderer.shadowMap.enabled = true
renderer.shadowMap.type = THREE.PCFSoftShadowMap
renderer.outputEncoding = THREE.sRGBEncoding
renderer.toneMappingExposure = 2
console.log(renderer.info);

/**
 * Animate
 */
const clock = new THREE.Clock()
let previousTime = 0

const stats = new Stats()
document.body.appendChild(stats.dom)

/* Liner Interpolation
 * lerp(min, max, ratio)
 * eg,
 * lerp(20, 60, .5)) = 40
 * lerp(-20, 60, .5)) = 20
 * lerp(20, 60, .75)) = 50
 * lerp(-20, -10, .1)) = -.19
 */
function lerp(x, y, a) {
    return (1 - a) * x + a * y;
}

// Used to fit the lerps to start and end at specific scrolling percentages
function scalePercent(start, end) {
    return (scrollPercent - start) / (end - start);
}

const animationScripts = [];

// First Truck Trip
animationScripts.push({
    start: 0,
    end: 67,
    func: () => {
        truck.position.x = scrollPercent * 3;
        frontWheels.position.x = scrollPercent * 3 + 0.70
        frontWheels.rotation.z =  - Math.PI * scrollPercent / 3.25
        backWheels.rotation.z =  - Math.PI * scrollPercent / 3.25
        backWheels.position.x = scrollPercent * 3 - 1
        camera.position.x = truck.position.x;
        camera.position.z = 16;
    },
});

// Truck Parked, Camera moving
animationScripts.push({
    start: 67,
    end: 75,
    func: () => {
        camera.position.x = lerp(truck.position.x, 205.65, scalePercent(67, 75));
        camera.position.y = lerp(2.5, 0.750, scalePercent(67, 75));
        camera.position.z = lerp(16, -0.5, scalePercent(67, 75));
    },
});

// Camera moving behind truck
animationScripts.push({
    start: 76,
    end: 86,
    func: () => {
        camera.position.x = 205.65
        camera.rotation.y = lerp(0, - Math.PI / 2, scalePercent(76, 86))
        camera.position.z = lerp(-0.5, 12, scalePercent(76, 86))
        camera.position.y = lerp(0.75, 2.0, scalePercent(76, 86))
        camera.position.x = lerp(205.65, truck.position.x - 4, scalePercent(76, 86))
        truck.position.x = 200.771
        frontWheels.position.x = truck.position.x + 0.70
        backWheels.position.x = truck.position.x - 1
    },
});

// Truck and camera moving towards stop sign
animationScripts.push({
    start: 87,
    end: 95,
    func: () => {
        truck.position.x = lerp(200.771, 213.5, scalePercent(87, 95))
        camera.position.x = truck.position.x - 4
        frontWheels.position.x = truck.position.x + 0.70
        backWheels.position.x = truck.position.x - 1
    },
});

// Camera moving towards billboard
animationScripts.push({
    start: 95,
    end: 100,
    func: () => {
        camera.position.x = lerp(209.5, 215, scalePercent(95, 100))
    },
});

function playScrollAnimations() {
    animationScripts.forEach((a) => {
        if (scrollPercent >= a.start && scrollPercent < a.end) {
            a.func()
        }
    })
}

let scrollPercent = 0

document.body.onscroll = () => {
    //calculate the current scroll progress as a percentage
    scrollPercent =
        ((document.documentElement.scrollTop || document.body.scrollTop) /
            ((document.documentElement.scrollHeight || document.body.scrollHeight) - document.documentElement.clientHeight)) * 100;
}

/**
 * Modals
 */
let modalIsOpen = false
function openModal(id) {
    document.getElementById(id).classList.add('open');
    document.body.classList.add('jw-modal-open');
    modalIsOpen = true
}

// close currently open modal
function closeModal() {
    if (modalIsOpen){
        document.querySelector('.jw-modal.open').classList.remove('open');
        document.body.classList.remove('jw-modal-open');
        modalIsOpen = false
    }
}

// close modals on background click
window.addEventListener('load', function() {
    document.addEventListener('click', event => {
        if (event.target.classList.contains('jw-modal')) {
            closeModal();
        }
    });
});

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()
    const deltaTime = elapsedTime - previousTime
    previousTime = elapsedTime

    if (scrollPercent === 0 ) {
        camera.position.set(3, 2.5, 16)
    }

    // Intro Section
    if (scrollPercent <= 11 && introElement.style.visibility !== 'visible' && loaded === true) {
        introElement.style.visibility = 'visible';
        thawdElement.style.visibility = 'hidden'
    }

    if (scrollPercent >= 16) {
        introElement.style.visibility = 'hidden'
    }

    // THAWD Section
    if (scrollPercent > 11 && scrollPercent <= 18 && thawdElement.style.visibility !== 'visible') {
        introElement.style.visibility = 'hidden';
        thawdElement.style.visibility = 'visible';
        hiCrush2017Element.style.visibility = 'hidden';
    }

    // Hi-Crush 2017 Section
    if (scrollPercent > 18 && scrollPercent <= 37 && hiCrush2017Element.style.visibility !== 'visible'){
        thawdElement.style.visibility = 'hidden';
        hiCrush2017Element.style.visibility = 'visible';
        preferredSandsElement.style.visibility = 'hidden';
    }

    // Preferred Sands Section
    if (scrollPercent > 37 && scrollPercent <= 51 && preferredSandsElement.style.visibility !== 'visible') {
        hiCrush2017Element.style.visibility = 'hidden';
        preferredSandsElement.style.visibility = 'visible';
        hiCrush2020Element.style.visibility = 'hidden';
    }

    // Hi-Crush 2020 Section
    if (scrollPercent > 51 && scrollPercent <= 64 && hiCrush2020Element.style.visibility !== 'visible') {
        preferredSandsElement.style.visibility = 'hidden';
        hiCrush2020Element.style.visibility = 'visible';
        homeElement.style.visibility = 'hidden';
    }

    // House Section
    if (scrollPercent > 64 && scrollPercent <= 75 && homeElement.style.visibility !== 'visible') {
        hiCrush2020Element.style.visibility = 'hidden';
        homeElement.style.visibility = 'visible';
    }

    if (scrollPercent >= 75) {
        homeElement.style.visibility = 'hidden'
    }

    if (scrollPercent >= 75 && scrollPercent < 76) {
        camera.position.set(205.63, 0.755, -0.4527)
        camera.rotation.y = 0
    }

    if (scrollPercent >= 75.5 && scrollPercent <= 76) {
        openModal('modal')
    }

    if (scrollPercent < 75.5 || scrollPercent > 77) {
        closeModal()
    }

    if (scrollPercent >= 86) {
        camera.position.z = 12
    }

    if (scrollPercent >= 99.8) {
        openModal('email-modal')
    }

    // Update controls
    // controls.update()

    playScrollAnimations()

    stats.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()