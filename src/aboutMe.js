import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import * as CANNON from 'cannon'
import gsap from 'gsap'

/**
 * Mobile
 */
let isMobile = function () {
    let check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

// Flashlight Effect
const shadow = document.querySelector('.shadow');
document.addEventListener('mousemove', (e) => {
    let x = e.clientX - (document.documentElement.clientWidth * 1.5);
    let y = e.clientY - (document.documentElement.clientHeight * 1.5);
    shadow.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
})

// Changing subtitles
const words = ['Software Engineer', 'Problem Solver', 'Lifelong Learner', 'Leader', 'Human'];
const changingWord = document.getElementById('changing-word');
let currentIndex = 0;

function changeWord() {
    changingWord.textContent = words[currentIndex];
    currentIndex = (currentIndex + 1) % words.length;

    setTimeout(() => {
        changingWord.classList.add('fade-out');
        setTimeout(() => {
            changingWord.classList.remove('fade-out');
            changeWord();
        }, 1000); // 1000ms for fade-out duration
    }, 1000); // 2000ms for the time each word is displayed
}
changeWord();

/**
 * Base
 */
// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Textures
 */
const textureLoader = new THREE.TextureLoader()

const cubeTextureLoader = new THREE.CubeTextureLoader()
const pythonTexture = textureLoader.load('/textures/skillsTextures/pythonTexture.jpg')
const reactTexture = textureLoader.load('/textures/skillsTextures/reactTexture.jpg')
const dockerTexture = textureLoader.load('/textures/skillsTextures/dockerTexture.jpg')
const cssTexture = textureLoader.load('/textures/skillsTextures/cssTexture.jpg')
const microservicesTexture = textureLoader.load('/textures/skillsTextures/microservicesTexture.jpg')
const javascriptTexture = textureLoader.load('/textures/skillsTextures/javascriptTexture.jpg')
const reduxTexture = textureLoader.load('/textures/skillsTextures/reduxTexture.jpg')
const fastAPITexture = textureLoader.load('/textures/skillsTextures/fastAPITexture.jpg')
const htmlTexture = textureLoader.load('/textures/skillsTextures/htmlTexture.jpg')
const databasesTexture = textureLoader.load('/textures/skillsTextures/databasesTexture.jpg')
const messagePassingTexture = textureLoader.load('/textures/skillsTextures/messagePassingTexture.jpg')
const djangoTexture = textureLoader.load('/textures/skillsTextures/djangoTexture.jpg')
const threeJSTexture = textureLoader.load('/textures/skillsTextures/threeJSTexture.jpg')

const skillsTextures = [
    pythonTexture,
    reactTexture,
    dockerTexture,
    cssTexture,
    microservicesTexture,
    javascriptTexture,
    reduxTexture,
    fastAPITexture,
    htmlTexture,
    databasesTexture,
    messagePassingTexture,
    djangoTexture,
    threeJSTexture
]

let environmentMap
if (isMobile()) {
    environmentMap = cubeTextureLoader.load([
        '/textures/environmentMaps/mobile/px.png',
        '/textures/environmentMaps/mobile/nx.png',
        '/textures/environmentMaps/mobile/py.png',
        '/textures/environmentMaps/mobile/ny.png',
        '/textures/environmentMaps/mobile/pz.png',
        '/textures/environmentMaps/mobile/nz.png',
    ])
} else {
    console.log('desktop');
    environmentMap = cubeTextureLoader.load([
        '/textures/environmentMaps/desktop/px.png',
        '/textures/environmentMaps/desktop/nx.png',
        '/textures/environmentMaps/desktop/py.png',
        '/textures/environmentMaps/desktop/ny.png',
        '/textures/environmentMaps/desktop/pz.png',
        '/textures/environmentMaps/desktop/nz.png',
    ])
}

/**
 * Physics
 */
// World
const world = new CANNON.World()
world.broadphase = new CANNON.SAPBroadphase(world)
world.allowSleep = true
world.gravity.set(0, -9.82, 0)

// Materials
const defaultMaterial = new CANNON.Material('default')
const defaultContactMaterial = new CANNON.ContactMaterial(
    defaultMaterial,
    defaultMaterial,
    {
        friction: 0.1,
        restitution: 0.75
    }
)
world.addContactMaterial(defaultContactMaterial)
world.defaultContactMaterial = defaultContactMaterial


// Floor
const floorShape = new CANNON.Plane()
const floorBody = new CANNON.Body()
floorBody.position.y = -14
floorBody.mass = 0
floorBody.addShape(floorShape)
floorBody.quaternion.setFromAxisAngle(
    new CANNON.Vec3(-1, 0, 0),
    Math.PI * 0.5
)
world.addBody(floorBody)

// Left wall
const leftWallShape = new CANNON.Box(new CANNON.Vec3(0.5, 15, 5));
const leftWallBody = new CANNON.Body({ mass: 0 });
leftWallBody.addShape(leftWallShape);
leftWallBody.position.set(-4, -4, -2.5);
world.addBody(leftWallBody);

// Right wall
const rightWallShape = new CANNON.Box(new CANNON.Vec3(0.5, 15, 5));
const rightWallBody = new CANNON.Body({ mass: 0 });
rightWallBody.addShape(rightWallShape);
rightWallBody.position.set(4, -4, -2.5);
world.addBody(rightWallBody);

// Back wall
const backWallShape = new CANNON.Box(new CANNON.Vec3(5, 15, 0.5));
const backWallBody = new CANNON.Body({ mass: 0 });
backWallBody.addShape(backWallShape);
backWallBody.position.set(0, -4, -4);
world.addBody(backWallBody);

// Front wall
const frontWallShape = new CANNON.Box(new CANNON.Vec3(5, 15, 0.5));
const frontWallBody = new CANNON.Body({ mass: 0 });
frontWallBody.addShape(frontWallShape);
frontWallBody.position.set(0, -4, 0.5);
world.addBody(frontWallBody);


/**
 * Three.js
 */

const material = new THREE.MeshStandardMaterial({
    metalness: 1,
    roughness: 0,
    envMap: environmentMap
})

// Models
const objectDistance = 4
let modelsLoaded = false
const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('/draco/')

const gltfLoader = new GLTFLoader()
gltfLoader.setDRACOLoader(dracoLoader)


let hardhat = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    material
)

let pickaxe = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    material
)

let truck = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    material
)

let dog = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    material
)

let car = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    material
)

const sectionMeshes = [pickaxe, truck, dog, car]


gltfLoader.load(
    '/models/hardhat.glb',
    (gltf) => {
        hardhat = gltf.scene.children[0]
        hardhat.scale.set(0.5, 0.5, 0.5)
        hardhat.position.set(0, - objectDistance - 1, 1)
        hardhat.rotation.y = Math.PI
        scene.add(hardhat)

        pickaxe = gltf.scene.children[0]
        pickaxe.scale.set(0.05, 0.05, 0.05)
        pickaxe.position.y = - objectDistance * 3 + 0.5
        pickaxe.position.x = 1.5
        pickaxe.position.z = 1
        pickaxe.material = material
        scene.add(pickaxe)
        sectionMeshes[0] = pickaxe

        truck = gltf.scene.children[0]
        truck.scale.set(truck.scale.x * 0.66, truck.scale.y * 0.66, truck.scale.z * 0.66)
        truck.position.set(-2, - objectDistance * 5.1, 0)
        truck.material = material
        scene.add(truck)
        sectionMeshes[1] = truck

        dog = gltf.scene.children[0]
        dog.scale.set(0.025, 0.025, 0.025)
        dog.position.set(-2, - objectDistance * 5.85, 0)
        dog.material = material
        sectionMeshes[2] = dog
        scene.add(dog)

        car = gltf.scene.children[0]
        car.scale.set(0.2, 0.25, 0.25)
        car.position.set(-2, - objectDistance * 6.6, 0)
        car.material = material
        sectionMeshes[3] = car
        scene.add(car)

        modelsLoaded = true
    }
)


/**
 * Floor
 */
const floor = new THREE.Mesh(
    new THREE.PlaneGeometry(50, 10),
    new THREE.MeshStandardMaterial({
        color: 'black',
        side: THREE.DoubleSide
    })
)
floor.receiveShadow = true
floor.rotation.x = - Math.PI * 0.5
floor.position.y = - objectDistance * 3 - 2
scene.add(floor)

/**
 * Lights
 */
const directionalLight = new THREE.DirectionalLight(0xffffff, 0.4)
directionalLight.castShadow = true
directionalLight.shadow.mapSize.set(1024, 1024)
directionalLight.shadow.camera.far = 20
directionalLight.shadow.camera.left = - 7
directionalLight.shadow.camera.top = 7
directionalLight.shadow.camera.right = 7
directionalLight.shadow.camera.bottom = - 7
directionalLight.position.set(5, 5, 5)
scene.add(directionalLight)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Group
const cameraGroup = new THREE.Group()
scene.add(cameraGroup)

// Base camera
const camera = new THREE.PerspectiveCamera(35, sizes.width / sizes.height, 0.1, 100)
camera.position.z = 6
cameraGroup.add(camera)

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    alpha: true
})
renderer.shadowMap.enabled = true
renderer.shadowMap.type = THREE.PCFSoftShadowMap
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Scroll
 */
let scrollPercent = 0
let currentSelection = null

function spinObject(currentSelection) {
    gsap.to(
        sectionMeshes[currentSelection].rotation,
        {
            duration: 1.5,
            ease: 'power2.inOut',
            x: '+= 6',
            y: '+= 3'
        }
    )
}

window.addEventListener('scroll', () => {
    //calculate the current scroll progress as a percentage
    scrollPercent =
        ((document.documentElement.scrollTop || document.body.scrollTop) /
            ((document.documentElement.scrollHeight || document.body.scrollHeight) - document.documentElement.clientHeight)) * 100;

    if (scrollPercent >= 30 && scrollPercent < 50) {
        currentSelection = 0
        spinObject(currentSelection)
    }

    if (scrollPercent >= 54 && scrollPercent < 61) {
        currentSelection = 1
        spinObject(currentSelection)
    }

    if (scrollPercent >= 63 && scrollPercent < 70) {
        currentSelection = 2
        spinObject(currentSelection)
    }

    if (scrollPercent >= 72 && scrollPercent < 78) {
        currentSelection = 3
        spinObject(currentSelection)
    }
})

/**
 * Cursor
 */
const cursor = {}
cursor.x = 0
cursor.y = 0

window.addEventListener('mousemove', (event) => {
    cursor.x = event.clientX / sizes.width - 0.5
    cursor.y = event.clientY / sizes.height - 0.5
})

// Mouse for lookAt
const mouse = new THREE.Vector2()

window.addEventListener('mousemove', (_event) => {
    mouse.x = _event.clientX / sizes.width * 2 - 1
    mouse.y = - (_event.clientY / sizes.height) * 2 + 1
})

/**
 * Utils
 */
const objectsToUpdate = []

const sphereGeometry = new THREE.SphereGeometry(1, 20, 20)

const createSphere = (radius, position, material) => {
    // Three.js mesh
    const mesh = new THREE.Mesh(sphereGeometry, material)
    mesh.scale.set(radius, radius, radius)
    mesh.castShadow = true
    mesh.position.copy(position)
    scene.add(mesh)

    // Cannon.js body
    const shape = new CANNON.Sphere(radius)

    const body = new CANNON.Body({
        mass: 1,
        position: new CANNON.Vec3(0, 3, 0),
        shape: shape,
        material: defaultMaterial
    })
    body.position.copy(position)
    // body.addEventListener('collide', playHitSound)
    world.addBody(body)

    // Save in objectsToUpdate
    objectsToUpdate.push({
        mesh: mesh,
        body: body
    })
}

/**
 * Animate
 */
const clock = new THREE.Clock()
let oldElapsedTime = 0
let spheresFallen = false

const tick = () => {
    const elapsedTime = clock.getElapsedTime()
    const deltaTime = elapsedTime - oldElapsedTime
    oldElapsedTime = elapsedTime

    // Update physics world
    world.step(1 / 60, deltaTime, 3)

    for (const object of objectsToUpdate) {
        object.mesh.position.copy(object.body.position)
        object.mesh.quaternion.copy(object.body.quaternion)
    }

    // Animate Camera
    camera.position.y = - scrollY / sizes.height * 4

    const parallaxX = cursor.x * 0.5
    const parallaxY = - cursor.y * 0.5
    cameraGroup.position.x += (parallaxX - cameraGroup.position.x) * 2.1 * deltaTime
    cameraGroup.position.y += (parallaxY - cameraGroup.position.y) * 2.1 * deltaTime

    // Animate meshes
    if (modelsLoaded) {
        for (const mesh of sectionMeshes) {
            mesh.rotation.x += deltaTime * 0.1
            mesh.rotation.y += deltaTime * 0.12
        }
    }

    // Animate hardhat
    const location = new THREE.Vector3(mouse.x, mouse.y - objectDistance)
    hardhat.lookAt(location)

    if (currentSelection === 0 && spheresFallen === false) {
        spheresFallen = true
        for (let i = 0; i < skillsTextures.length; i++) {
            createSphere(
                0.5,
                {
                    x: (Math.random() - 0.5) * 3,
                    y: 3 + i,
                    z: - (Math.random() - 0.5) * 3 - 2,
                },
                new THREE.MeshStandardMaterial(
                    {
                        metalness: 1,
                        roughness: 0.1,
                        map: skillsTextures[i],
                        envMap: environmentMap
                    }
                )
            )
        }
    }

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()